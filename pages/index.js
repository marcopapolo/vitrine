import Head from 'next/head'
// import Loader from '../components/loader'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>BAAN</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="welcome">
          <div className="banner">
            <div className="left">
              <div className="brand">BAAN</div>
              <h1>Combine micro chatbots into a fully operational assistant</h1>
              <h2>Switch chatbot in your conversation seamlessly</h2>
              <div className="actions">
                <a href={`${process.env.NEXT_PUBLIC_DASHBOARD_URL || ''}/get-started`}>Join the beta</a>
                <a href={`${process.env.NEXT_PUBLIC_DASHBOARD_URL || ''}/signin`}>Sign in</a>
              </div>
            </div>
            <div className="right">
              <div className="chat flexbox">
                <div className="messages column flexgrow overflow p-small">
                  <div className="text-xs mt-small flex-row-reverse ml-normal mr-small">
                    Sales asking question
                  </div>
                  <div className="message sent flex-row-reverse ml-normal mr-small">
                    {/* <div className="context text-xs">Your first bot is responding</div> */}
                    <div className="text p-small">How far from my target am I for this quarter?</div>
                  </div>
                  <div timer="1" className="bot1 botname flexbox mt-small mr-normal ml-small text-xs">
                    Verification identity bot
                  </div>
                  <div timer="1" className="bot1 message received flexbox mr-normal ml-small">
                    <div className="text p-small">Let me confirm your identity first. I have sent you an Okta notification.</div>
                  </div>
                  <div timer="2" className="bot1 message received flexbox mr-normal ml-small">
                    <div className="text p-small">Thank you for confirming I am talking to Frank.</div>
                  </div>
                  <div timer="3" className="bot2 botname flexbox mt-small mr-normal ml-small text-xs">
                    Salesforce bot
                  </div>
                  <div  timer="3" className="bot2 message received flexbox mr-normal ml-small">
                    <div className="text p-small">You are still $59,000 short for Q1</div>
                  </div>
                  {/* <div timer="4" className="botname message sent flex-row-reverse ml-normal mr-small">
                    <div className="text p-small">Loading...</div>
                  </div> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      {/* <footer>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel" className="logo" />
        </a>
      </footer> */}

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}

{/* <style jsx>{`
    .container {
      min-height: 100vh;
      padding: 0 0.5rem;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    main {
      padding: 5rem 0;
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    footer {
      width: 100%;
      height: 100px;
      border-top: 1px solid #eaeaea;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    footer img {
      margin-left: 0.5rem;
    }

    footer a {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    a {
      color: inherit;
      text-decoration: none;
    }

    .title a {
      color: #0070f3;
      text-decoration: none;
    }

    .title a:hover,
    .title a:focus,
    .title a:active {
      text-decoration: underline;
    }

    .title {
      margin: 0;
      line-height: 1.15;
      font-size: 4rem;
    }

    .title,
    .description {
      text-align: center;
    }

    .description {
      line-height: 1.5;
      font-size: 1.5rem;
    }

    code {
      background: #fafafa;
      border-radius: 5px;
      padding: 0.75rem;
      font-size: 1.1rem;
      font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
        DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
    }

    .grid {
      display: flex;
      align-items: center;
      justify-content: center;
      flex-wrap: wrap;

      max-width: 800px;
      margin-top: 3rem;
    }

    .card {
      margin: 1rem;
      flex-basis: 45%;
      padding: 1.5rem;
      text-align: left;
      color: inherit;
      text-decoration: none;
      border: 1px solid #eaeaea;
      border-radius: 10px;
      transition: color 0.15s ease, border-color 0.15s ease;
    }

    .card:hover,
    .card:focus,
    .card:active {
      color: #0070f3;
      border-color: #0070f3;
    }

    .card h3 {
      margin: 0 0 1rem 0;
      font-size: 1.5rem;
    }

    .card p {
      margin: 0;
      font-size: 1.25rem;
      line-height: 1.5;
    }

    .logo {
      height: 1em;
    }

    @media (max-width: 600px) {
      .grid {
        width: 100%;
        flex-direction: column;
      }
    }
  `}</style> */}
