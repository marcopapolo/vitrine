// import './loader.module.css'
import clsx from 'clsx'

function Loader ({ className="" }) {
 return (<div class={clsx("loader-5", className)}><span></span></div>)
}

export default Loader